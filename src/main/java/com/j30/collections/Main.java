package com.j30.collections;

public class Main {

    public static void main(String[] args){
        MyArrayList list = new MyArrayList();

        list.add("abc");
        list.add("def");
        list.add(1);
        list.add(6.0);
        list.add(6.8);
        list.add(6.7);
        list.add(6.6);
        list.add(6.5);
        list.add(6.4);
        list.add(6.3);
        list.add(6.2);
        list.add(6.1);
        list.add(6.11);
        list.add(6.12);

        System.out.println(list);
        list.remove(8);
        System.out.println(list);
        list.remove(8);
        System.out.println(list);
        list.remove(8);
        System.out.println(list);

    }
}
