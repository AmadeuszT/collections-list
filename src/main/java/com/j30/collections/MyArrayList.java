package com.j30.collections;

public class MyArrayList {

    private static final int INITIAL_ARRAY_SIZE = 10;
    private Object[] array;
    private int size = 0; // rozmiar listy na poczatku

    public MyArrayList() {
        this(INITIAL_ARRAY_SIZE);
    }

    public MyArrayList(int InitialArraySize) {
        array = new Object[InitialArraySize];
    }


    // dodanie elementu na koniec
    public void add(Object element) {
        checkSizeAndExtendNeeded();
        array[size++] = element;

    }

    private void checkSizeAndExtendNeeded() {
        if (size >= array.length) {
            Object[] newArray = new Object[array.length * 2];

            // przepisujemy elelemtny
            for (int i = 0; i < array.length; i++) {
                newArray[i] = array[i];
            }
            array = newArray; // nie isteresuje mnie juz stara, tylko nowa
        }
    }


    public void remove(int indeks) {
        if (indeks > 0 && indeks < size) {
            for (int i = indeks; i < size - 1; i++) {
                array[i] = array[i + 1];
            }
            array[--size] = null;
        } else {
            throw new IndexOutOfBoundsException("Invalid index: " + indeks);
        }
    }

    @Override
    public String toString() {

        StringBuilder sb = new StringBuilder("(");

        if (size > 0) {
            for (int i = 0; i < size; i++) {
                sb.append(array[i]);
                if (i != size - 1) {
                    sb.append(", ");
                }
            }
        }
        sb.append(")");
        return sb.toString();
    }


}